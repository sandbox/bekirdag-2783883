<?php 

/**
 * Implements hook_drush_command().
 */
function field_search_drush_command(){
  $items['find-videos'] = array(
      'description' => 'Find videos in body, blocks and other node fields.',
      'arguments' => array(
          'phrase' => 'Search phrase',
      ),
      'aliases' => array('fv'),
      'examples' => array(
          'drush fv "my phrase"' => 'Search a phrase',
      ),
  );
  return $items;
}

function drush_field_search($phrase){
  $node_types = node_type_get_types();
  drush_print("Searching node fields");
  drush_print("-----------------");
  foreach ($node_types as $key=>$node_type) {
  	drush_print("Node type: ".$key);
  	drush_print("-----------------");
  	$data=field_info_instances("node",$key);
  	foreach ($data as $field_name=>$field_value) {
  		if($field_value["widget"]["type"]=="text_textfield")
  		{
  		  drush_print("Searching field: ". $field_name);
  		  field_search_search_field($field_name,$phrase);
  		}
  	}
  }
  drush_print("Searching body fields");
  drush_print("-----------------");
  field_search_search_field("body",$phrase);
}

function field_search_search_field($field,$phrase)
{
  $table_name = "field_data_".$field;
  $search_field_name = $field."_value";
  
  $phrases = array($phrase);
  
  foreach ($phrases as $phrase) {
  	$q = db_query("select entity_id from ".$table_name." where ". $search_field_name . " like :pattern",array(":pattern"=> "%". db_like($phrase) . "%"));
  	
  	while ($row = $q->fetchAssoc()) {
  	  drush_print("entity id: ". $row["entity_id"]);
  	}
  }
}